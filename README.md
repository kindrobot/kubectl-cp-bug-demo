# kubectl-cp-bug-demo

A simple repo for reproducing a bug whereby a `kubectl cp` times out in a
Gitlab runner when going through the Gitlab k8s-proxy
